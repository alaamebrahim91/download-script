<?php
/**
 * Created by alaa mohammed
 * 28-11-2018 : 04:57 PM
 */


spl_autoload_register(function ($className) {
    $classPath =  $className . '.php';
    if(file_exists($classPath)) {
        include($classPath);
    }
});


$fileManager = new Download();
$helper = new Helpers();

if($_GET){
    $file_id = $_GET['file_id'];
    $file_id = $helper->CleanString($file_id);
} else {
    include './404/404.php';
    return false;
}

$fileManager->download($file_id);