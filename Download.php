<?php


class Download {

    protected $fileName = './files.json';
    protected $notFoundPage = './404/404.php';
    /**
     * @var FilesModel
     */
    protected $model;

    protected $data;

    public function __construct(){}

    /**
     * Read json file
     */
    public function readFile() {
        if(file_exists($this->fileName)) {
            try {
                $sfile = file_get_contents($this->fileName);
                $this->data = json_decode($sfile);
                if(!is_array($this->data) || empty($this->data)) {
                    $this->FireErrorPage();
                    return false;
                }
            } catch (Exception $ex) {
                error_log("Error reading file: " . $ex->getMessage());
                $this->FireErrorPage();
            }
        } else {
            $this->FireErrorPage();
            return false;
        }
    }

    /**
     * Download file using its id in json file
     * @param $file_id
     * @return bool
     */
    public function download($file_id)
    {
        $this->readFile();

        if($file_id != null && $this->IdExists($file_id) == true) {
            $this->PerformDownload($file_id);
        } else {
            $this->FireErrorPage();
            return false;
        }
    }

    /**
     * Checks if given file id exists in json file
     * @param $file_id
     * @return bool
     */
    public function IdExists($file_id) {
        $exists = false;
        foreach ($this->data as $file) {
            if($file->id == $file_id) {
                $exists = true;
            }
        }
        return $exists;
    }

    /**
     * The actual code for downloading a file
     * @param $file_id
     * @return void #downloaded file
     */
    public function PerformDownload($file_id) {
        foreach ($this->data as $file) {
            if($file->id != 'undefined' && $file->id == $file_id) {
                try{
                    header('Content-Type: '. $file->fileType);
                    header("Content-Transfer-Encoding: Binary");
                    header("Content-disposition: attachment; filename=\"" . $file->fileName . "\"");
                    readfile($file->url);
                } catch (Exception $ex){
                    error_log($ex->getMessage());
                    $this->FireErrorPage();
                }
            }
        }
    }

    public function FireErrorPage(){
        if(file_exists($this->notFoundPage)) {
            include $this->notFoundPage;
        } else {
            echo 'Permission Denied!!';
        }
    }
}