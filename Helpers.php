<?php


class Helpers {

    public function CleanString($string) {
        $string = str_replace(' ', '-', $string);
        $string = $this->CleanHtmlSpecialCharacters($string);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

    public function CleanHtmlSpecialCharacters($string) {
        return htmlspecialchars(strip_tags($string));;
    }
}